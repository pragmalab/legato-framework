package legato.utils
{
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;
	import mx.rpc.events.FaultEvent;
	
	public class UniversalResponder implements IResponder
	{
		public static function pushResult(token:AsyncToken, targetObject:Object, propertyName:String):UniversalResponder
		{
			var responder:UniversalResponder = new UniversalResponder(targetObject, propertyName);
			token.addResponder(responder);
			return responder;
		}
		
		
		private var targetObject:Object;
		
		private var propertyName:String;
		
		public function UniversalResponder(targetObject:Object, propertyName:String)
		{
			this.targetObject = targetObject;
			this.propertyName = propertyName;
		}
		
		public function result(event:Object):void
		{
			this.targetObject[propertyName] = event.result;
		}
		
		public function fault(event:Object):void
		{
				
		}	
	}
}