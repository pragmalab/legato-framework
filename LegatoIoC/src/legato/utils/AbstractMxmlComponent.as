package legato.utils
{
	import mx.core.IMXMLObject;
	
	/**
	 * Empty non-visual mxml component.  
	 * @author Piotr
	 * 
	 */
	public class AbstractMxmlComponent implements IMXMLObject
	{
		public function initialized(document:Object, id:String):void
		{
		}
		
	}
}