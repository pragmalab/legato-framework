package legato.injection
{
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	
	
	/**
	 * Simple wrapper used to register simple objects in depedency injection container
	 * @author Piotr
	 * @see IDIComponent 
	 */
	public class WrapperComponent implements IDIComponent
	{
		
		/**
		 * Use it to create new instance of <code>WrapperComponent</code> 
		 * @param component
		 * @return 
		 * 
		 */
		public static function create(component:Object):WrapperComponent
		{
			var newWrapper:WrapperComponent = new WrapperComponent();
			newWrapper.innerComponent = component;
			return newWrapper;
		}
		
		
		private var _innerComponent:Object;
		/**
		 * Component scope. One of <i>application</i>, <i>context</i> or <i>instance</i>.
		 * See <code>DIComponentScope</code> for more information.<br/>
		 * Implementation of <code>IDIComponent</code>.
		 * @return 
		 * 
		 */				
		public function get scope():String
		{
			return DIComponentScope.APPLICATION_SCOPE;
		}
		/**
		 * wrapped component instance 
		 * @return 
		 * 
		 */		
		public function get innerComponent():Object
		{
			return _innerComponent;
		}
	
		public function set innerComponent(val:Object):void
		{
			_innerComponent=val;
		}
		
		/**
		 * Class of wrapped component.
		 * @return 
		 * 
		 */
		public function get componentClass():Class 
		{
			return getDefinitionByName(getQualifiedClassName(this.innerComponent)) as Class;
		}
	
		
		/**
		 * Returns always the wrapped component. <code>context</code> parameter is always ignored here.
		 * Use <code>DIComponent</code> to make use of context-related functionalities.<br/>
		 * Implementation of <code>IDIComponent</code>
		 * @param context context name is ignored in <code>WrapperComponent</code>
		 * @return 
		 * 
		 */
		public function getInstance(context:String = null):Object
		{
			return _innerComponent;
		}
		
        public function removeInstance(context:String = null):void
        {
			throw new Error("Cannot remove instance of wrapped simple component of type: " + getQualifiedClassName(this.innerComponent) + ". Use DIComponent instead");
        }		

	}
}