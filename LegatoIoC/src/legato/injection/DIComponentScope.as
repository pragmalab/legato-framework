package legato.injection
{
	
	/**
	 * Scope names used by InjectionContainer
	 * @author Piotr
	 * @see InjectionContainer
	 * @see DIComponent
	 */
	public class DIComponentScope
	{
		/**
		 * Means that only one instance of component is created in application.
		 * */
		public static const APPLICATION_SCOPE:String = "application";
		/**
		 * Means that one instance of component is created for each context 
		 * (based on context name passed in <code>getComponentbyId</code> or <code>getComponentByInterface</code> methods 
		 * of <code>InjectionContainer</code>) 
		 * */
		public static const CONTEXT_SCOPE:String = "context";
		/**
		 * New instance of component is created on every request.
		 * */
		public static const INSTANCE_SCOPE:String = "instance";
	}
}