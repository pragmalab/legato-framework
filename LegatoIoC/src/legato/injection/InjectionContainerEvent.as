package legato.injection
{
	import flash.events.Event;
	
	/**
	 * Event class used by InjectionContainer 
	 * @author Piotr
	 * @see InjectionContainer
	 */
	public class InjectionContainerEvent extends Event
	{
		
		public static const INJECTION_INIT_COMPLETE:String = "injectionInitComplete";
		public static const INJECTION_INIT_START:String = "injectionInitStart";
		
		public function InjectionContainerEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false):void
		{
			super(type, bubbles, cancelable);	
		}

	}
}