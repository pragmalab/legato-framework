package legato.injection
{
	
	/**
	 * Basic interface for depedency injection pattern implementation; objects of this interface 
	 * are used to create application components. 
	 * @author Piotr
	 * @see DIComponentScope
	 */
	public interface IDIComponent
	{
		
		/**
		 * Component scope. One of <i>application</i>, <i>context</i> or <i>instance</i>.
		 * See <code>DIComponentScope</code> for more information.
		 * @return 
		 * 
		 */
		function get scope():String;
		
		/**
		 * Class of the component. 
		 * @return 
		 * 
		 */
		function get componentClass():Class;
		
		/**
		 * Creates new instance of component in given context.
		 * Context parameter is used only witch <code>scope</code> set to <i>context</i>. 
		 * You can use <code>null</code> context - works like <i>application</i> scope.
		 * @param context name of the context
		 * @return component instance
		 * 
		 */
		function getInstance(context:String = null):Object;
		
		
		/**
		 * Removes instance of component from specified context 
		 * @param context
		 * 
		 */
		function removeInstance(context:String = null):void;
	
	}
}