package legato.injection
{
	
	/**
	 * Class used to find InjectionContainer. 
	 * Example of usage:<br/><br/>
	 * <code>
	 * var container:InjectionContainer = DILocator.getContainer();
	 * </code> 
	 * @author Piotr
	 * @see InjectionContainer
	 */
	public class DILocator
	{
		private static var containerInstance:InjectionContainer = null;
		
		/**
		 * @return Initialized InjectionContainer class.
		 */
		public static function getContainer():InjectionContainer
		{
			if (containerInstance == null)
			{
				throw new Error("Injection container not initialized");
			}	
			
			return containerInstance;	
		}
		
		/**
		 * InjectionContainer uses it in initialization. Saves reference to container. 
		 * Can be used only once.
		 * @param container InjectionContainer class
		 * 
		 */
		internal static function initializeContainer(container:InjectionContainer):void
		{
			if (containerInstance != null)
			{
				throw new Error("Injection container already initialized as "+containerInstance);
			}
			containerInstance = container;
		}
		
	}
}