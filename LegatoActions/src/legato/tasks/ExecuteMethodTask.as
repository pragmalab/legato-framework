package legato.tasks
{
	import flash.events.Event;
	
	/**
	 * Simple task that executes given method. Just set method and object parameters. 
	 * @author Piotr
	 * 
	 */
	public class ExecuteMethodTask extends AbstractTask
	{
		
		
		
		private var _method:Function;
		private var _params:Array;
		private var _object:Object;

		/**
		 * The method to be executed.
		 * @return 
		 * 
		 */
		public function get method():Function
		{
			return _method;
		}
	
		public function set method(val:Function):void
		{
			_method=val;
		}
	
		/**
		 * Method parameters. 
		 * @param val
		 * 
		 */
		public function get params():Array
		{
			return _params;
		}
	
		public function set params(val:Array):void
		{
			_params=val;
		}
		
		/**
		 * Object that contains the method.  
		 * @return 
		 * 
		 */
		public function get object():Object
		{
			return _object;
		}
	
		public function set object(val:Object):void
		{
			_object=val;
		}

		
		/**
		 * Constructor 
		 * 
		 */
		public function ExecuteMethodTask()
		{
			
		}
		
		/**
		 * Creates new task. 
		 * @param object
		 * @param method
		 * @param params
		 * @return 
		 * 
		 */
		public static function createTask(object:Object, method:Function, params:Array = null):ExecuteMethodTask
		{
			var task:ExecuteMethodTask = new ExecuteMethodTask();
			task.method = method;
			task.params = params;
			task.object = object;	
			return task;
		}
		
		public override function execute():void
		{
			this._method.apply(this.object,this.params);
			this.dispatchEvent(new Event(Event.COMPLETE));
		}
	}
}