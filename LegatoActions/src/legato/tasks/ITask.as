package legato.tasks
{
	import flash.events.IEventDispatcher;
	
	
	/**
	 * Basic interface for tasks. 
	 * @author Piotr
	 * 
	 */
	public interface ITask extends IEventDispatcher
	{
		function execute():void;
	}
}