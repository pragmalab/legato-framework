package legato.tasks
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	[Event(name="complete", type="flash.events.Event")]
	/**
	 * Basic abstract class for all tasks. 
	 * @author Piotr
	 * 
	 */
	public class AbstractTask extends EventDispatcher implements ITask
	{

		/**
		 * Constructor 
		 * 
		 */
		public function AbstractTask()
		{
		}
		
		/**
		 * Executes task. 
		 * 
		 */
		public function execute():void
		{
			
		}
	}
}