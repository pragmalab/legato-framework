package legato.actions
{
	import legato.tasks.ITask;
	
	import flash.events.Event;
	
	/**
	 * Basic interface for all actions. 
	 * @author Piotr
	 * 
	 */
	public interface IAction extends ITask
	{
		
		/**
		 *  
		 * The name of action. Used to identify resources in ActionsFramework.properties file
		 * 
		 */
		function get name():String;
	
		function set name(val:String):void;
		
		/**
		 *  
		 * Icon used by visual components
		 * 
		 */
		function get icon():*;
	
		function set icon(val:*):void;
	
		/**
		 * Tooltip used by visual components
		 * 
		 */
		function get tooltipText():String;
	
		function set tooltipText(val:String):void;

		/**
		 * Label used by visual componens, for example button label
		 * 
		 */
		function get label():String;
	
		function set label(val:String):void;
		
		/**
		 * If set to true action is duplicated before execution and the copy is stored in TaskManager 
		 * 
		 */
		function get saveCopy():Boolean;
		
		function set saveCopy(val:Boolean):void;
		
		/**
		 * Starts execution of an action. This method is used by visual components to run an action.
		 * @param event the event object prpagated from visual component, for example mose click event
		 * 
		 */
		function invoke(event:Event):void
		
		/**
		 * Creates a duplicate of an action before save. Used when saveCopy is set to true. 
		 * @return copy of an action
		 * 
		 */
		function createCopy():IAction;
	}
}