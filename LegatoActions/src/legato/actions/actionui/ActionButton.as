package legato.actions.actionui
{
	import legato.actions.AbstractAction;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.Button;
	import mx.events.PropertyChangeEvent;


	/**
	 * Button prepared to work with actions. 
	 * @author Piotr
	 * 
	 */
	public class ActionButton extends Button
	{
		private var _action:AbstractAction;

		private var _showActionLabel:Boolean = true;
		
		private var _showActionIcon:Boolean = true;
		
		private var _showActionTooltip:Boolean = true;

		[Inspectable(category="Common")]
		/**
		 * An action used by button. Button gets label, icon and tooltip from action and invokes it on mouse click.
		 * @return 
		 * 
		 */
		public function get action():AbstractAction
		{
			return this._action;
		}
	
		public function set action(val:AbstractAction):void
		{
			if (this._action != null)
			{
				this.removeEventListener(MouseEvent.CLICK, _action.invoke);
			}
			this._action=val;
			this.addEventListener(MouseEvent.CLICK, _action.invoke);
			this._action.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE,this.updateView);
			this.updateView(null);
			
		}
		
		[Bindable]
		[Inspectable(category="Common")]
		/**
		 * If set to true button gets label from action. Default is true.
		 * @return 
		 * 
		 */
		public function get showActionLabel():Boolean
		{
			return _showActionLabel;
		}
	
		public function set showActionLabel(val:Boolean):void
		{
			_showActionLabel=val;
		}

		[Bindable]
		[Inspectable(category="Common")]	
		/**
		 * If set to true button gets icon from action. Default is true.
		 * @return 
		 * 
		 */
		public function get showActionIcon():Boolean
		{
			return _showActionIcon;
		}
	
		public function set showActionIcon(val:Boolean):void
		{
			_showActionIcon=val;
		}

		[Bindable]
		[Inspectable(category="Common")]	
		/**
		 * If set to true button gets tooltip from action. Default is true. 
		 * @return 
		 * 
		 */
		public function get showActionTooltip():Boolean
		{
			return _showActionTooltip;
		}
	
		public function set showActionTooltip(val:Boolean):void
		{
			_showActionTooltip=val;
		}
		
		private function updateView(event:Event):void
		{
		
			if (this._action != null)
			{
				this.label = this._showActionLabel ? this._action.label != "" ? this._action.label : ("["+this._action.name+"]") : "";	
				if (this._showActionIcon)
				{
					this.setStyle("icon",this._action.icon);
				}
				else
				{
					this.setStyle("icon",null);
				}
				
				this.toolTip = this._showActionTooltip ? this._action.tooltipText : "";
			}	
		}
		
	}
}