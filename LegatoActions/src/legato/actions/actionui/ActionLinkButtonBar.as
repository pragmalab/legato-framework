package legato.actions.actionui
{
	import legato.actions.AbstractAction;
	
	import mx.collections.IList;
	import mx.containers.Box;
	import mx.controls.Button;
	
	/**
	 * LinkButton bar created from array of actions.
	 * @author Piotr
	 * 
	 */
	public class ActionLinkButtonBar extends Box
	{
		private var buttons:Array;
		
		private var _actionsArray:Object;
		
		private var _showIcons:Boolean = true;
		
		private var _showLabels:Boolean = true;
		
		private var _showTooltips:Boolean = true;
		
		/**
		 * Array of actions.
		 * @return 
		 * 
		 */
		public function get actionsArray():Object
		{
			return _actionsArray;
		}
	
		public function set actionsArray(val:Object):void
		{
			if (!(val is IList || val is Array))
			{
				throw new Error("actionsArray property should be Array of mx.collections.IList subclass.");
			}
			_actionsArray=val;
			this.rebuild();
		}
		
		/**
		 * If set to true buttons get icons from actions. Default is true.
		 * @return 
		 * 
		 */
		public function get showIcons():Boolean
		{
			return _showIcons;
		}
	
		public function set showIcons(val:Boolean):void
		{
			if (_showIcons!=val)
			{
				_showIcons=val;
				this.resetButtonsProperties();
			}
		}
	
		/**
		 * If set to true buttons get labels from actions. Default is true.
		 * @return 
		 * 
		 */
		public function get showLabels():Boolean
		{
			return _showLabels;
		}
	
		public function set showLabels(val:Boolean):void
		{
			if (_showLabels!=val)
			{
				_showLabels=val;
				this.resetButtonsProperties();
			}
		}
		
		/**
		 * If set to true buttons get tooltips from actions. Default is true. 
		 * @return 
		 * 
		 */
		public function get showTooltips():Boolean
		{
			return _showTooltips;
		}
	
		public function set showTooltips(val:Boolean):void
		{
			if (_showTooltips!=val)
			{
				_showTooltips=val;
				this.resetButtonsProperties();
			}
		}	
		
		/**
		 * Constructor 
		 * 
		 */
		public function ActionLinkButtonBar()
		{
			super();
			this.buttons = new Array();
		}
		
		private function rebuild():void
		{
			this.removeAllChildren();
			this.buttons = new Array();
			if (this._actionsArray != null)
			{
				for(var i:String in this._actionsArray)
				{
					var newButton:ActionLinkButton = new ActionLinkButton();
					newButton.action = AbstractAction(this._actionsArray[i]);
					newButton.showActionIcon = this.showIcons;
					newButton.showActionLabel = this.showLabels;
					newButton.showActionTooltip = this.showTooltips;
					this.buttons.push(newButton);
					this.addChild(newButton);
				}	
			}
		}
		
		private function resetButtonsProperties():void
		{
			for(var i:int = 0; i < this.buttons.length; i++)
			{
				var b:ActionLinkButton = ActionLinkButton(this.buttons[i]);
				b.showActionIcon = this.showIcons;
				b.showActionLabel = this.showLabels;
				b.showActionTooltip = this.showTooltips;
			}	
		}
		
	}
}