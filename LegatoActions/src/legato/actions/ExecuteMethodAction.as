package legato.actions
{
	import legato.tasks.ExecuteMethodTask;
	
	import flash.events.Event;
	
	/**
	 * Simple action that executes given method. Just set method and object parameters.
	 * This action uses ExecuteMethodTask behind.
	 * @author Piotr
	 * 
	 */
	public class ExecuteMethodAction extends AbstractAction
	{
		
		private var innerTask:ExecuteMethodTask;
		
		public function set method(val:Function):void
		{
			this.innerTask.method = val;
		}
		
		/**
		 * The method to be executed.
		 * @return 
		 * 
		 */
		public function get method():Function
		{
			return this.innerTask.method;
		}
		
		public function set object(val:Object):void
		{
			this.innerTask.object = val;
		}
		
		/**
		 * Object that contains the method.  
		 * @return 
		 * 
		 */
		public function get object():Object
		{
			return this.innerTask.object;
		}
		
		/**
		 * Method parameters. 
		 * @param val
		 * 
		 */
		public function set params(val:Array):void
		{
			this.innerTask.params = val;
		}
		
		public function get params():Array
		{
			return this.innerTask.params;
		}
		 
		/**
		 * Construcotr. 
		 * 
		 */
		public function ExecuteMethodAction()
		{
			super();
			this.innerTask = new ExecuteMethodTask();
		}
		
		/**
		 * Creates new action. 
		 * @param name 
		 * @param object
		 * @param method
		 * @param params
		 * @param saveCopy
		 * @return 
		 * 
		 */
		public static function createAction(name:String, object:Object, method:Function, params:Array = null, saveCopy:Boolean = true):ExecuteMethodAction
		{
			var act:ExecuteMethodAction = new ExecuteMethodAction();
			act.name = name;
			act.object = object;
			act.method = method;
			act.params = params;
			act.saveCopy = saveCopy;
			return act;
		}
		
		public override function execute():void
		{
			this.innerTask.execute();
			this.dispatchEvent(new Event(Event.COMPLETE));
		}
		
		public override function createCopy():IAction
		{
			var actionCopy:ExecuteMethodAction = ExecuteMethodAction(super.createCopy());
			actionCopy.innerTask = this.innerTask;
			return actionCopy; 	
		}
		
	}
}