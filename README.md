# Legato Framework for Flex #

Simple, lightweight application framework for Adobe Flex (ActionScript 3) based on some design patterns used in J2EE and Java Swing Application Framework. 

There are two separate modules:

* *Legato Actions* - extension of well known command pattern to make tasks and actions.
* *Legato Injection* - Depedency Injection Container based on MXML configuration and metadata injection

## Legato Actions

Actions framework solves some common tasks and problems that exist during rich application development:

  * multiple access methods to functionalities (by button, menu item, context menu etc.),
  * building multilingual interfaces,
  * building sequences of tasks (for example to show dialog and save document before closing it ), organizing business components.

An [example](http://tester.done.pl/pgw/blog/ActionsExample/ActionsExample.html) and see [the source](http://tester.done.pl/pgw/blog/ActionsExample/srcview/).

## Features:

### Multilingual support ###
Names, icons, tooltips are loaded from resources file 

    addAction.label=Add
    addAction.tooltipText=Add task
    addAction.icon=Embed('../../icons/add.png')


Changing language on runtime changes actions and visual components language.


### Actions aware visual components
Just add your action to button, menu or context menu - visual components will take the look from action and execute it on click.

    <actionui:ActionButton id="button1" action="{someAction}">
    </actionui:ActionButton>



*Read more on LegatoActions*

## Legato Injection 

It's an implementation of the pattern as described by Martin Fowler in "Inversion of Control Containers and the Dependency Injection pattern". It contains setter injection and constructor injection implementations. Main ideas are to :

* connect simple components in depedency injection container,
* make use of Flex features e.g. use MXML files (with code completition etc.) rather than external XML files for depedency injection configuration

* this container can be used also as a service locator. 

## Features:

### Simple MXML configuration 
for both setter injection and constructor injection

   

        <DIComponent id="component1" scope="application" componentClass="{ExplicitCommandImpl}"
    >
        <constructorParams> 
         <mx:Array>
          <mx:String>string parameter</mx:String>
          <mx:Number>2</mx:Number>
          <mx:Object>{component2}</mx:Object>
         </mx:Array>
        </constructorParams>
        </DIComponent>
    
        <DIComponent id="component2" scope="application" componentClass="{SimpleComponent}" />
    
        <actions:AbstractAction id="action1" name="addAction" />
      
        <actions:AbstractAction id="action2" name="copyAction"/>

### Metadata Injection
Example:

    //Injection by id
    [Inject(id="component1")]
    public var a:Object ;
    
    //injection by id simpler
    [Inject("component2")]
    public var b:Object;
    
    //injection by type 
    [Inject(type="poc.SimpleComponent2")]
    public var c:SimpleComponent2;
    
    //Injection by type guess
    [Inject]
    public var d:ExplicitCommandImpl;
    
    
    //Injection by type works with interfaces, basic classess too
    [Inject]
    public var e:IExplicitCommand;


*Read more on LegatoInjection.*
